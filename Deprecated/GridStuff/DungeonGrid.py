import numpy as np
import cv2

class DungeonGrid:
    GRIDSQ = 10
    DIM = 500
    def __init__(self): #This will add Justin's fancy list later
        self.img = np.zeros((self.DIM,self.DIM,3), np.uint8)
        self.img.fill(255)
        self.counter = 0
         # Draw a diagonal blue line with thickness of 5 px (THIS WILL BE REMOVED)
        cv2.line(self.img,(0,0),(499,499),(255,0,0),5)
        
        for x in range(self.DIM):
            for y in range(self.DIM):
                if x % self.GRIDSQ == 0 or y % self.GRIDSQ == 0:
                    self.img[x][y] = (0,0,0) 

    
    def drawDungeon(self, roomData):
        roomData = self.sortRoomData(roomData)
        for room in roomData:
            print(str(room.roomName) + " " + str(room.locRelToEntry))
        
        
        for index in range(len(roomData)):
            if roomData[index].locRelToEntry == (0,0):
               self.drawRoom(roomData[0], (0, 0)) 
            elif roomData[index].locRelToEntry[0] == 0 and roomData[index].locRelToEntry[1] != 0:
                self.drawRoom(roomData[index], (roomData[index-1].roomSize[0]+2, 0))            
            elif roomData[index].locRelToEntry[1] == 0 and roomData[index].locRelToEntry[0] != 0:
                self.drawRoom(roomData[index], (0, self.leftRooms(roomData[index], roomData)+2))
            elif roomData[index].locRelToEntry[0] > 0 and roomData[index].locRelToEntry[1] > 0:
                self.drawRoom(roomData[index], (self.belowRooms(roomData[index], roomData) + 2, self.leftRooms(roomData[index], roomData)+2))
    
    def belowRooms(self, room, roomData):
        size = 0
        print("\n\nRoom Compare  " + room.roomName + " " + str(room.locRelToEntry))
        for tmpRoom in roomData:
            if tmpRoom.locRelToEntry[0] == room.locRelToEntry[0] and tmpRoom.locRelToEntry[1] < room.locRelToEntry[1]:
                size += tmpRoom.roomSize[0] + 2
                print("Room below  " + tmpRoom.roomName + " " + str(tmpRoom.locRelToEntry))
        print("Size " + str(size))
        return size
    
    
    def leftRooms(self, room, roomData):
        size = 0
        tmpSize = 0
        for yAxis in range(room.locRelToEntry[1] + 1): #roomData.max[0] This will be changed later to grab the number properly
            for tmpRoom in roomData:
                if tmpRoom.locRelToEntry[0] < room.locRelToEntry[0] and tmpRoom.locRelToEntry[1] == yAxis:
                    tmpSize += tmpRoom.roomSize[0] + 2 
                    print("\n\nRoom Compare  " + room.roomName + " " + str(room.locRelToEntry))
                    print("Room to the Left  " + tmpRoom.roomName + " " + str(tmpRoom.locRelToEntry))
                    
            if tmpSize > size:
                size = tmpSize
                tmpSize = 0
                
        print("Size " + str(size))
        return size - 2
        
    def drawRoom(self, room, corner):
        print(str(room.roomName) + " " + str(room.locRelToEntry))
        for w in range(room.roomSize[0]):
            for h in range(room.roomSize[1]):
                self.drawAtCoordinate(corner[0] + w, corner[1] + h)

        self.counter+=1       
        self.labelRoom(corner[1] + (room.roomSize[1] // 2), corner[0] + (room.roomSize[0] // 2))
        
    
    def labelRoom(self, x, y):
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(self.img, str(self.counter), ((x*self.GRIDSQ), self.DIM - (y*self.GRIDSQ)), font, .3, (255,255, 255), 1, cv2.LINE_AA)
        print(str(x*self.GRIDSQ) + " " + str(self.DIM - (y*self.GRIDSQ)) + " " + str(self.counter))
    
    def sortRoomData(self, roomData):
        sortedData = []
        for x in range(4 + 1): #roomData.max[0] This will be changed later to grab the number properly
            for y in range(4 + 1):
                for index in range(len(roomData)):
                    if roomData[index].locRelToEntry == (x, y):
                        sortedData.append(roomData.pop(index))
                        #print(str(sortedData[-1].roomName))
                        break
        return sortedData
    
    def getImg(self):
        return self.img
    
    

    def drawAtCoordinate(self, x, y):
        for pointX in range(self.DIM - ((x+1) * self.GRIDSQ) + 1, self.DIM - (x * self.GRIDSQ)):
            for pointY in range((y * self.GRIDSQ) + 1, (y+1) * self.GRIDSQ):
                self.img[pointX][pointY] = (0,0,255)

