import unittest
import DungeonMapData as mapData

class MiniRoom:
    def __init__(self, name, x, y, w, h, connections):
        self.max = (4, 4)
        self.roomName = name
        self.locRelToEntry = (x, y)
        self.roomSize = (w, h)
        self.connections = connections


class TestGridStructure(unittest.TestCase):



    def setUp(self):
        self.finalGrid = ([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [2, 2, 2, 2, 2, 0, 0, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [2, 2, 2, 2, 2, 0, 0, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [2, 2, 2, 2, 2, 0, 0, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [2, 2, 2, 2, 2, 0, 0, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 5, 5, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 5, 5, 0, 0],
        [1, 1, 1, 1, 1, 0, 0, 3, 3, 3, 3, 3, 0, 0, 5, 5, 5, 5, 5, 5, 5, 0, 0],
        [1, 1, 1, 1, 1, 0, 0, 3, 3, 3, 3, 3, 0, 0, 5, 5, 5, 5, 5, 5, 5, 0, 0],
        [1, 1, 1, 1, 1, 0, 0, 3, 3, 3, 3, 3, 0, 0, 5, 5, 5, 5, 5, 5, 5, 0, 0],
        [1, 1, 1, 1, 1, 0, 0, 3, 3, 3, 3, 3, 0, 0, 5, 5, 5, 5, 5, 5, 5, 0, 0],
        [1, 1, 1, 1, 1, 0, 0, 3, 3, 3, 3, 3, 0, 0, 5, 5, 5, 5, 5, 5, 5, 0, 0]])


        self.roomData = []
        self.roomData.append(MiniRoom("Entry Hall", 3, 0, 7, 7, ["Up", "Right"]))
        self.roomData.append(MiniRoom("Large Hall", 2, 0, 7, 7, ["Up", "Right"]))
        self.roomData.append(MiniRoom("Encounter", 3, 1, 5, 5, ["Up"]))
        self.roomData.append(MiniRoom("Empty", 4, 0, 4, 4, []))
        self.roomData.append(MiniRoom("Trap", 1, 0, 5, 5, ["Up", "Right"]))
        self.roomData.append(MiniRoom("Empty 2", 2, 1, 5, 5, ["Right"]))
        self.roomData.append(MiniRoom("Locked", 3, 2, 7, 7, ["Up"]))
        self.roomData.append(MiniRoom("Loot", 0, 0, 5, 5, ["Up", "Right"]))
        self.roomData.append(MiniRoom("Key", 1, 1, 4, 4, ["Up", "Right"]))
        self.roomData.append(MiniRoom("Empty 3", 2, 2, 3, 3, ["Right"]))
        self.roomData.append(MiniRoom("Secret", 3, 3, 5, 5, ["Up"]))
        self.roomData.append(MiniRoom("Empty 4", 0, 1, 5, 5, []))
        self.roomData.append(MiniRoom("Loot 2", 2, 3, 7, 7, ["Up", "Right"]))
        self.roomData.append(MiniRoom("Empty 5", 2, 4, 9, 9, []))



    def testGridDrawing(self):
        dataMap = mapData.DungeonMapData()
        dataMap.plotDungeon(self.roomData)
        self.assertEqual(self.finalGrid, dataMap.grid)


if __name__ == "__main__":
    unittest.main()
