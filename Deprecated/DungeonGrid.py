import numpy as np
import cv2
import math

class DungeonGrid:
    GRIDSQ = 10
    DIM = 600
    def __init__(self, dungeon):
        self.DIM = self.GRIDSQ * dungeon.getMaxDimension() * (dungeon.getLargestRoomSize() + 3)

        self.img = np.zeros((self.DIM,self.DIM,3), np.uint8)
        self.img.fill(255)
        self.counter = 0
        self.dungeon = dungeon

        for x in range(self.DIM):
            for y in range(self.DIM):
                if x % self.GRIDSQ == 0 or y % self.GRIDSQ == 0:
                    self.img[x][y] = (0,0,0)


    def drawDungeon(self):
        roomData = self.sortRoomData(self.dungeon.rooms)

        #DEBUG CODE
        #for room in roomData:
            #print(str(room.roomName) + " " + str(room.locRelToEntry) + " " + str(room.roomID))

        for index in range(len(roomData)):
            if roomData[index].locRelToEntry == (0,0): # is first room?
                self.drawRoom(roomData[0], (0, 0))
            elif roomData[index].locRelToEntry[0] == 0 and roomData[index].locRelToEntry[1] != 0: #Column 1?
                self.drawRoom(roomData[index], (roomData[index-1].roomSize[0]+2, 0))
            elif roomData[index].locRelToEntry[1] == 0 and roomData[index].locRelToEntry[0] != 0: #Row 1?
                self.drawRoom(roomData[index], (0, self.leftRooms(roomData[index], roomData)+2))
            elif roomData[index].locRelToEntry[0] > 0 and roomData[index].locRelToEntry[1] > 0:#Everything else
                self.drawRoom(roomData[index], (self.belowRooms(roomData[index], roomData) + 2, self.leftRooms(roomData[index], roomData)+2))

            #Label rooms at the end to avoid room labels getting overwritten
            self.labelRooms()

    #Stupidly simplified drawing method in case complex one fails
    #Too much space between rooms with this method
    def drawDungeonSimple(self):
        roomData = self.sortRoomData(self.dungeon.rooms)

        #Scale up coordinates system base on largest room + buffer between rooms
        largestRoomSize = self.dungeon.getLargestRoomSize()
        for room in self.dungeon.rooms:
            #Draw room using local coordinates * upscale value based on largest room
            self.drawRoom(room, (room.locRelToEntry[1] * (largestRoomSize + 2), room.locRelToEntry[0] * (largestRoomSize + 2)))

        #Label rooms at the end to avoid room labels getting overwritten
        self.labelRooms()

    #Wrapper function for labelRoom that calls labelRoom for each room in the DungeonGrid
    #Used after every room has already been drawn
    def labelRooms(self):
        for room in self.dungeon.rooms:
            self.labelRoom((room.drawCoords[1] + (room.roomSize[1] // 2)), (room.drawCoords[0] + (room.roomSize[0] // 2)), room.roomID)

    def belowRooms(self, room, roomData):
        size = 0
        #print("\n\nRoom Compare  " + room.roomName + " " + str(room.locRelToEntry))    #DEBUG
        for tmpRoom in roomData:
            if tmpRoom.locRelToEntry[0] == room.locRelToEntry[0] and tmpRoom.locRelToEntry[1] < room.locRelToEntry[1] and room.locRelToEntry[1] == 1:
                size += tmpRoom.roomSize[0]
            elif tmpRoom.locRelToEntry[0] == room.locRelToEntry[0] and tmpRoom.locRelToEntry[1] < room.locRelToEntry[1] and room.locRelToEntry[1] > 1:
                size += tmpRoom.roomSize[0] + 1
                #print("Room below  " + tmpRoom.roomName + " " + str(tmpRoom.locRelToEntry)) #DEBUG
        #print("Size " + str(size)) #DEBUG
        return size


    def leftRooms(self, room, roomData):
        size = 0
        tmpSize = 0
        for yAxis in range(room.locRelToEntry[1] + 1): #roomData.max[0] This will be changed later to grab the number properly
            for tmpRoom in roomData:
                if tmpRoom.locRelToEntry[0] < room.locRelToEntry[0] and tmpRoom.locRelToEntry[1] == yAxis:
                    tmpSize += tmpRoom.roomSize[0] + 2
                    #print("\n\nRoom Compare  " + room.roomName + " " + str(room.locRelToEntry))    #DEBUG
                    #print("Room to the Left  " + tmpRoom.roomName + " " + str(tmpRoom.locRelToEntry))  #DEBUG

            if tmpSize > size:
                size = tmpSize
                tmpSize = 0

        #print("Size " + str(size)) #DEBUG
        return size - 2

    def drawRoom(self, room, corner):
        #DEBUG
        #print(str(room.roomName) + " " + str(room.locRelToEntry))

        #When we find the corner the room needs to be drawn from,
        #I am saving it into a variable so it can be used later
        #I do this for both the sorted version and original data structure
        self.dungeon.getRoomByID(room.roomID).drawCoords = corner
        room.drawCoords = corner

        for w in range(room.roomSize[0]):
            for h in range(room.roomSize[1]):
                self.drawAtCoordinate(corner[0] + w, corner[1] + h)

        self.drawHallways(room, corner)

        self.counter+=1
        #self.labelRoom(corner[1] + (room.roomSize[1] // 2), corner[0] + (room.roomSize[0] // 2), room.roomID)

    #We might need to find a better way to determine hallway length instead of using magic number 4
    def drawHallways(self, room, corner):
        connections = self.dungeon.getConnectingDirections(room)

        if connections == []:
            return
        if "Up" in connections:
            hallwayStart = (corner[0] + room.roomSize[0], corner[1] + (room.roomSize[1] // 2))
            for x in range(hallwayStart[0], hallwayStart[0] + 5):
                self.drawAtCoordinate(x, hallwayStart[1])

        if "Right" in connections:
            hallwayStart = (corner[0] + (room.roomSize[0] // 2), corner[1] + room.roomSize[1])
            for y in range(hallwayStart[1], hallwayStart[1] + 5):
                self.drawAtCoordinate(hallwayStart[0], y)

    def labelRoom(self, x, y, id):
        font = cv2.FONT_HERSHEY_SIMPLEX
        if id > 0:
            cv2.putText(self.img, str(id), ((x*self.GRIDSQ), self.DIM - (y*self.GRIDSQ)), font, .3, (255,255, 255), 1, cv2.LINE_AA)
        #DEBUG CODE
        #print(str(x*self.GRIDSQ) + " " + str(self.DIM - (y*self.GRIDSQ)) + " " + str(self.counter))

    def sortRoomData(self, roomData):
        sortedData = []
        for x in range(self.dungeon.getMaxDimension() + 1):
            for y in range(self.dungeon.getMaxDimension() + 1):
                for index in range(len(roomData)):
                    if roomData[index].locRelToEntry == (x, y):
                        sortedData.append(roomData[index]) #This is why hallways were broken, dont pop off the dungeon object
                        break
        return sortedData

    def getImg(self):
        return self.img

    def drawAtCoordinate(self, x, y):
        for pointX in range(self.DIM - ((x+1) * self.GRIDSQ) + 1, self.DIM - (x * self.GRIDSQ)):
            for pointY in range((y * self.GRIDSQ) + 1, (y+1) * self.GRIDSQ):
                self.img[pointX][pointY] = (0,0,255)
