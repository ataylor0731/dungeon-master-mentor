# -*- coding: utf-8 -*-
"""
Created on Fri Feb  2 15:35:40 2018

@author: anthony.taylor
"""
import cv2
import DungeonGraph as dg
import DungeonGrid as grid

#dungeon = DL.DungeonLayout()
#dungeon.generateDungeon()
#dungeon.drawDungeonGraph()

dungeon = dg.DungeonGraph("Small")
dungeon.generateDungeon()
dungeon.populateDungeon()
dungeon.printDungeon()
dungeon.drawDungeonGraph()


#grid = grid.DungeonGrid(dungeon)
#grid.drawDungeonSimple()
#cv2.imshow('img', grid.getImg())
#cv2.waitKey(0)
#cv2.destroyAllWindows()
