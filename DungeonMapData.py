# -*- coding: utf-8 -*-
"""
Created on Tue Mar 27 11:25:49 2018

@author: anthony.taylor
"""
import numpy as np
import cv2

BUFFER = 3
SMHALL = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
SMHALLCNT = (1, 1)


class DungeonMapData:


	def __init__(self, dungeonData):
		self.grid = []
		self.SMHALL = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
		self.currY = 0
		self.currMaxWidth = 0
		self.prevMaxWidth = 0
		self.corners = {}
		self.dungeonData = dungeonData


	def plotDungeon(self):
		self.roomData = self.sortRoomData(self.dungeonData.rooms)

		for index in range(len(self.roomData)):
			if self.roomData[index].locRelToEntry[1] == 0:
				self.cleanGrid()

			self.plotRoom(self.roomData[index])


		print("Finished Grid: ")
		self.cleanGrid() #This is to finalize the last grid padding
		print(str(self.corners))
		self.drawHallways()
		self.drawGrid()
		self.labelRooms();
		
		cv2.imwrite('map.png',self.img)
		
		
		
	def drawHallways(self):
		self.dungeonData.rooms = self.roomData
		for index in range(len(self.roomData)):
		
			connections = self.dungeonData.getConnectingDirections(self.roomData[index])
			print("Connections: " + str(connections))
			
			if "Up" in connections:
				endRoom = self.dungeonData.getRoomByID(connections["Up"])
				self.drawUpHallway(self.roomData[index], endRoom)
				
			if "Right" in connections:
				endRoom = self.dungeonData.getRoomByID(connections["Right"])
				self.drawRightHallway(self.roomData[index], endRoom)
			
			
	def drawUpHallway(self, startRoom, endRoom):
		startCorner = self.corners[startRoom.roomID]
		endCorner = self.corners[endRoom.roomID]
		overlap = []
		
		for i in range(startRoom.roomSize[0]):
			endRange = [self.corners[endRoom.roomID][1], self.corners[endRoom.roomID][1] + endRoom.roomSize[0]]
			if startCorner[1] + i >= endRange[0] and startCorner[1] + i < endRange[1]:
				overlap.append(startCorner[1] + i)
				print("Overlap = " + str(overlap))

		else:
			xCoord = overlap[len(overlap) // 2]
			startLoop = startCorner[0] + startRoom.roomSize[0] + 1
				
		for y in range(startLoop, endCorner[0] + 1):
			self.grid[len(self.grid) - y][xCoord] = "H"
	
	
	def drawRightHallway(self, startRoom, endRoom):
		startCorner = self.corners[startRoom.roomID]
		endCorner = self.corners[endRoom.roomID]
		overlap = []
		
		for i in range(startRoom.roomSize[0]):
			endRange = [self.corners[endRoom.roomID][0], self.corners[endRoom.roomID][0] + endRoom.roomSize[0]]
			if startCorner[0] + i >= endRange[0] and startCorner[0] + i < endRange[1]:
				overlap.append(startCorner[0] + i)

		if len(overlap) == 0:
			middlePoint = (startCorner[1] + startRoom.roomSize[0] + endCorner[1]) // 2 # get middle point between rooms
			yCoord = len(self.grid) - (startCorner[0] + ((startRoom.roomSize[0]) // 2)) - 1 #y coord to travel to this middle point

			for xCoord in range(startCorner[1] + startRoom.roomSize[0], middlePoint + 1): #moves hallway to the right until reaches middle.
				self.grid[yCoord][xCoord] = "B"
		
			if startCorner[0] < endCorner[0]: #Checks to see if the room is higher up
				for y in range(startCorner[0] + (startRoom.roomSize[0] // 2), endCorner[0] + (endRoom.roomSize[0] // 2) + 1): # move hallway up until it reaches middle of end room
					self.grid[len(self.grid) - y - 1][middlePoint] = "B"
			
			elif startCorner[0] > endCorner[0]: #Checks to see if the room is lower
				for y in range(startCorner[0] + (startRoom.roomSize[0] // 2), endCorner[0] + (endRoom.roomSize[0] // 2) - 1, -1): # move hallway down until it reaches middle of end room
					self.grid[len(self.grid) - y - 1][middlePoint] = "B"
			
			startLoop = middlePoint + 1
			yCoord = len(self.grid) - (endCorner[0] + (endRoom.roomSize[0] // 2)) - 1
			
			
		else:	
			yCoord = len(self.grid) - (overlap[len(overlap) // 2] + 1)
			startLoop = startCorner[1] + startRoom.roomSize[0]
			
		for x in range(startLoop, endCorner[1]):
			self.grid[yCoord][x] = "H"
	
	def displayGrid(self):
		for row in self.grid:
			print("[", end='')
			for num in row:
				if len(str(num)) < 2 and len(self.roomData) >= 10:
					print("0" + str(num) + ", ", end='')
				else:
					print(str(num) + ", ", end='')
			print("]")
			
			
	def plotRoom(self, room):
		roomToAdd = []
		row = []
		bufferRow = []

		if room.roomID < 0:
			roomToAdd = SMHALL[:]
			roomToAdd[SMHALLCNT[0]][SMHALLCNT[1]] = room.roomID
			self.corners[room.roomID] = (self.currY + 1, self.prevMaxWidth + 1)
		
		else:
			for x in range(room.roomSize[0]):
				row.append(room.roomID)
				bufferRow.append(0)

			row.extend(list(np.zeros((BUFFER, ), np.uint8)))
			bufferRow.extend(list(np.zeros((BUFFER, ), np.uint8)))
			for y in range(room.roomSize[1]):
				roomToAdd.append(row)

			for buffer in range(BUFFER):
				roomToAdd.insert(0, bufferRow)

			print("Room To add: ")
			for row in roomToAdd:
				print(str(row))
			self.corners[room.roomID] = (self.currY, self.prevMaxWidth)

		for y in range(len(roomToAdd)):
			if len(self.grid) - self.currY == 0:
				self.grid.insert(0, (list(np.zeros((self.prevMaxWidth, ), np.uint8)) + roomToAdd.pop()))
			else:
				self.grid[len(self.grid) - self.currY - 1].extend(roomToAdd.pop())

			self.currY += 1

		if self.currMaxWidth <= room.roomSize[0] + BUFFER:
			self.currMaxWidth = room.roomSize[0] + BUFFER

	def cleanGrid(self):
		self.currY = 0
		self.prevMaxWidth += self.currMaxWidth
		self.padGrid()
		self.currMaxWidth = 0
		
	def padGrid(self):
		for index in range(len(self.grid)):
			if len(self.grid[index]) < self.prevMaxWidth:
				self.grid[index].extend(list(np.zeros((self.prevMaxWidth - len(self.grid[index]), ), np.uint8)))

	def sortRoomData(self, roomData):
		sortedData = []
		for x in range(self.dungeonData.getMaxDimension() + 1):
			for y in range(self.dungeonData.getMaxDimension() + 1):
				for index in range(len(roomData)):
					if roomData[index].locRelToEntry == (x, y):
						sortedData.append(roomData.pop(index))
						break
		return sortedData

		
	def labelRooms(self):
		keys = list(self.corners.keys())
		
		for index in range(len(keys)):
			x = (self.roomData[index].roomSize[0] // 2) + self.corners[keys[index]][1]
			y = (self.roomData[index].roomSize[1] // 2) + self.corners[keys[index]][0]
		
			self.labelRoom(x, y, keys[index])
			
	def labelRoom(self, x, y, id):
		font = cv2.FONT_HERSHEY_SIMPLEX
		if id > 0:
			cv2.putText(self.img, str(id), ((x*self.GRIDSQ)+ 2, self.DIMY - (y*self.GRIDSQ) - 2), font, .6, (255,255, 255), 1, cv2.LINE_AA)
		
	def drawGrid(self):
		self.prepGrid()
		for y in range(len(self.grid)):
			for x in range(len(self.grid[0])):
				if self.grid[y][x] != 0:
					#print(str(self.grid[y][x]))
					#print("Y INDEX: " + str(y) + "X INDEX: " + str(x))
					
					if self.grid[y][x] == "H":
						self.drawAtCoordinate(x, y, (255, 0, 0))
						
					elif self.grid[y][x] == "B":
						self.drawAtCoordinate(x, y, (0, 255, 0))
					else:
						self.drawAtCoordinate(x, y, (0, 0, 255))
	
	def prepGrid(self):
		self.GRIDSQ = 16
		self.DIMX = len(self.grid[0]) * self.GRIDSQ
		self.DIMY = len(self.grid) * self.GRIDSQ
		
		
		self.img = np.zeros((self.DIMY,self.DIMX,3), np.uint8)
		self.img.fill(255)
		
		#Draw Grid
		for x in range(self.DIMX):
			for y in range(self.DIMY):
				if x % self.GRIDSQ == 0 or y % self.GRIDSQ == 0:
					self.img[y][x] = (0,0,0) 
	
	def drawAtCoordinate(self, x, y, color):
		for pointX in range((x * self.GRIDSQ) + 1, (x * self.GRIDSQ) + self.GRIDSQ):
			for pointY in range((y * self.GRIDSQ) + 1, (y * self.GRIDSQ) + self.GRIDSQ):
				self.img[pointY][pointX] = color
		