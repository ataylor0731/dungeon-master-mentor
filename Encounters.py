import random


UNDERGROUND_1 = "data/encounterData.txt"
TRAP_DATA = "data/trapData.txt"
MECHANISM_DATA = "data/trapMechanismData.txt"
VISUALS_DATA = "data/descriptions/visuals.txt"
SECONDARIES_DATA = "data/descriptions/secondaries.txt"
DECORATIONS_DATA = "data/descriptions/decorations.txt"

#a parameter can be added later to choose which list to pull from
#difficulty mod is an integer that is added to the d100 roll, capped at 75
#difficulty modifier will increase as more encounters are generated in the dungeon by increments of 10

def rollDice(numDice, diceSize):
    count = 0
    for i in range(numDice):
        count += random.randint(1, diceSize)
    return count

def getRandomEncounter(diffMod, fileName):
    encNum = random.randint(1 + diffMod, 25 + diffMod)
    encString = ""

    data = getStringFromFile(encNum, fileName)
    for i in range(len(data)):
        encounter = data[i].split(",")
        count = rollDice(int(encounter[0]), int(encounter[1]))

        encString += str(count) + " " + encounter[2].split("\n")[0]

        if count > 1:
            encString += "s"

        encString += "\n"
    return encString[:-1]

def getRandomTrap(diffMod, levelBracket):
    diffNum = rollDice(1, diffMod + 25)
    trapNum = rollDice(1, 3)
    mechNum = rollDice(1, 5)
    trapString = ""
    trapType = ""

    data = getStringFromFile(trapNum, TRAP_DATA)
    type = data[0].split(',')
    trapString += type[0] + '\n'
    trapType = type[1]

    data = getStringFromFile(mechNum, MECHANISM_DATA)
    trapString += "Mechanism: " + data[0]

    trapString += getTrapDetails(levelBracket, diffNum, trapType)
    return trapString

def getTrapDetails(levelBracket, difficulty, type):
    infoString = ""

    #Set Difficulty Bracket
    if difficulty > 90:
        diffBracket = 3
    elif difficulty > 10:
        diffBracket = 2
    else:
        diffBracket = 1

    #Set details for attack vs save
    if type == "Attack":
        infoString += "Attack Bonus: "

        if diffBracket == 1:
            infoString += "+5\n"
        elif diffBracket == 2:
            infoString += "+8\n"
        elif diffBracket == 3:
            infoString += "+12\n"
    else:
        infoString += type[:-1] + " DC: "

        if diffBracket == 1:
            infoString += "10\n"
        elif diffBracket == 2:
            infoString += "15\n"
        elif diffBracket == 3:
            infoString += "+20\n"

    #Set damage value
    dmgID = levelBracket + diffBracket
    if dmgID == 2:
        infoString += "Damage: 1d10\n"
    elif dmgID == 3:
        infoString += "Damage: 2d10\n"
    elif dmgID == 4:
        infoString += "Damage: 4d10\n"
    elif dmgID == 5:
        infoString += "Damage: 10d10\n"
    elif dmgID == 6:
        infoString += "Damage: 18d10\n"
    else:
        infoString += "Damage: 24d10\n"

    return infoString

def getLoot():
    lootList = []
    toGen = getLootTableAmounts(rollDice(1,100), 1)

    if len(toGen) >= 1:
        lootList.append([toGen[0][2], rollDice(toGen[0][0], toGen[0][1])])

    if len(toGen) >= 2:
        magicItems = getMagicItems(getRandomNumberList(rollDice(toGen[1][0], toGen[1][1])), toGen[1][2])
        lootList += magicItems

    return convertLootListToString(lootList)

def getLootTableAmounts(probability, diffBracket):
    loot = []

    if diffBracket == 1:
        file = "data/loot/TreasureHoard1-4.txt"
    else:
        return None

    line = getStringFromFile(probability, file)
    data = line[0].split(',')
    if data[0] != '-':
        loot.append((int(data[0]), int(data[1]), data[2]))
    data = line[1].split(',')
    if data[0] != '-\n':
        loot.append((int(data[0]), int(data[1]), data[2][:-1]))

    return loot

def getRandomNumberList(tableRolls):
    numbers = []
    for i in range(tableRolls):
        numbers.append(rollDice(1,100))
    return numbers

def getMagicItems(lootValues, tableID):
    magicItems = []
    file = "data/loot/MagicItemsTable" + tableID + ".txt"

    for probability in lootValues:
        data = getStringFromFile(probability, file)[0][:-1]
        itemFound = False
        for item in magicItems:
            if item[0] in data:
                item[1] += 1
                itemFound = True
                break
        if itemFound == False:
            magicItems.append([data, 1])

    return magicItems

def convertLootListToString(lootList):
    lootString = ""
    for item in lootList:
        itemString = str(item[1]) + " "
        itemString += item[0] + "\n"
        lootString += itemString
    return lootString

def getRoomDescription():
    prob = rollDice(1,3)
    description = ""
    if prob < 3:
        description = getStringFromFile(rollDice(1,10), VISUALS_DATA)[0][:-1]
        if prob == 1:
            description += " " + getStringFromFile(rollDice(1,10), DECORATIONS_DATA)[0][:-1]
        else:
            description += " " + getStringFromFile(rollDice(1,10), SECONDARIES_DATA)[0][:-1]
    else:
        description = getStringFromFile(rollDice(1,10), DECORATIONS_DATA)[0][:-1]
        description += " " + getStringFromFile(rollDice(1,10), SECONDARIES_DATA)[0][:-1]

    return description

def getStringFromFile(probability, fileName):
    fd = open(fileName, 'r')
    for line in fd:
        data = line.split(":")
        numRange = data[0].split('-')
        if probability >= int(numRange[0]) and probability <= int(numRange[-1]):
            fd.close()
            return data[1:]
    fd.close()
    return None
