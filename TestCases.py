import RoomTypes
import math
import Encounters
import DungeonGraph
import unittest

#Test Entry Hall returns
class EntryHallTestCase(unittest.TestCase):

	def setUp(self):
		self.entry = RoomTypes.EntryRoom()

	def tearDown(self):
		pass


	def testGetRoom25(self):
		assert self.entry.getRoom(25) == "Large Hall", "getRoom returning incorrect Successor"

	def testGetRoom50(self):
		assert self.entry.getRoom(50) == "Large Hall", "getRoom returning incorrect Successor"

	def testGetRoom51(self):
		assert self.entry.getRoom(51) == "Encounter", "Get Room returning incorrect Successor"

	def testGetRoom80(self):
		assert self.entry.getRoom(80) == "Encounter", "Get Room returning incorrect Successor"

	def testGetRoom95(self):
		assert self.entry.getRoom(95) == "Locked", "Get Room returning incorrect Successor"

	def testGetRoom1000(self):
		assert self.entry.getRoom(1000) == "Locked", "Get Room returning incorrect Successor"

	def testGetRoomNeg1(self):
		assert self.entry.getRoom(-1) == "Large Hall", "Get Room returning incorrect Successor"



class TestRoom:
    def __init__(self, roomName, connectedRooms, maxConnections):
        self.roomName = roomName
        self.connectedRooms = connectedRooms
        self.maxConnections = maxConnections

class OpenNeighborTestCase(unittest.TestCase):

	def setUp(self):
		self.neighbors = []
		self.neighbors.append(TestRoom("Encounter", [1, 2], 3))
		self.neighbors.append(TestRoom("Large Hall", [1, 2, 3], 3))

		self.testRoom = RoomTypes.EntryRoom()

	def testOpenNeighborEncounter(self):
		assert self.testRoom.hasOpenNeighbor(self.neighbors, "Encounter") == True, "hasOpenNeighbor returning incorrectly"

	def testOpenNeighborLargeHall(self):
		assert self.testRoom.hasOpenNeighbor(self.neighbors, "Large Hall") == False, "hasOpenNeighbor returning incorrectly"

	def testOpenNeighborGarbage(self):
		assert self.testRoom.hasOpenNeighbor(self.neighbors, "Garbage") == False, "hasOpenNeighbor returning incorrectly"


class GenerateLootTestCase(unittest.TestCase):

	def testGetLootTableAmounts(self):
		assert Encounters.getLootTableAmounts(38, 1) == [(2, 6, "10gp Gems"), (1, 6, "A")]

	def testGetRandomNumberList(self):
		assert len(Encounters.getRandomNumberList(4)) == 4

	def testGetMagicItems(self):
		assert Encounters.getMagicItems([35], "A") == [['Potion of Healing', 1]]

	def testConvertLootListToString(self):
		assert Encounters.convertLootListToString([["Potion of Healing", 1]]) == "1 Potion of Healing\n"

class ContextGeneratorTestCase(unittest.TestCase):

	def testGetVisualDescriptor(self):
		vFile = "data/descriptions/visuals.txt"
		vString = "The walls are carved with ornate designs reminiscent of dwarven culture. They depict great warriors wielding legendary weapons.\n"
		assert Encounters.getStringFromFile(2, vFile) == [vString]

	def testGetSecondaryDescriptor(self):
		sFile = "data/descriptions/secondaries.txt"
		sString = "The area is ridden with an aura of sorrow and death. You feel as though you are being watched.\n"
		assert Encounters.getStringFromFile(3, sFile) == [sString]

	def testGetRoomDecoration(self):
		dFile = "data/descriptions/decorations.txt"
		dString = "A bubbling cauldron of warm green liquid sits in the center of the room.\n"
		assert Encounters.getStringFromFile(1, dFile) == [dString]

class RoomValidityTestCase(unittest.TestCase):

	def setUp(self):
		self.neighbors = []
		self.neighbors.append(TestRoom("Encounter", [1, 2], 3))
		self.neighbors.append(TestRoom("Large Hall", [1, 2, 3], 3))

		self.roomCounts = []
		self.roomCounts.append(("Entry", [0, 1]))
		self.roomCounts.append(("Large Hall", [10, math.inf]))
		self.roomCounts.append(("Encounter", [2, 2]))
		self.roomCounts.append(("Trap", [0, 1]))
		self.roomCounts.append(("Locked", [0, 1]))
		self.roomCounts.append(("Key", [1, 1]))
		self.roomCounts.append(("Loot", [0, 1]))
		self.roomCounts.append(("Secret", [0, 1]))
		self.roomCounts.append(("Empty", [0, math.inf]))
		self.roomCounts.append(("Small Hall", [0, math.inf]))
		self.roomCounts = dict(self.roomCounts)

		self.testRoom = RoomTypes.EntryRoom()

	def testValidRoomWithNeighbor(self):
		assert self.testRoom.checkRoomValid(self.neighbors, self.roomCounts, "Encounter") == True

	def testValidRoomByCount(self):
		assert self.testRoom.checkRoomValid(self.neighbors, self.roomCounts, "Large Hall") == True

	def testValidLockedRoom(self):
		assert self.testRoom.checkRoomValid(self.neighbors, self.roomCounts, "Locked") == True

	def testInvalidRoom(self):
		assert self.testRoom.checkRoomValid(self.neighbors, self.roomCounts, "Key") == False

class GenerateChildrenTestCase(unittest.TestCase):
	roomCounts = []
	roomCounts.append(("Entry", [0, 1]))
	roomCounts.append(("Large Hall", [0, math.inf]))
	roomCounts.append(("Encounter", [0, 2]))
	roomCounts.append(("Trap", [0, 1]))
	roomCounts.append(("Locked", [0, 1]))
	roomCounts.append(("Key", [0, 1]))
	roomCounts.append(("Loot", [0, 1]))
	roomCounts.append(("Secret", [0, 1]))
	roomCounts.append(("Empty", [0, math.inf]))
	roomCounts.append(("Small Hall", [0, math.inf]))
	roomCounts = dict(roomCounts)

	def testRoomGenerateChildren(self):
		neighbors = []
		testRoom = RoomTypes.generateRoomByTypeName("Entry")

		children = testRoom.generateChildRoomTypes(neighbors, self.roomCounts)
		assert len(children) > 0

		for key,value in self.roomCounts.items():
		    print(key + ": " + str(value))

if __name__ == "__main__":
	unittest.main()

print("\n\n~~~Testing Full Dungeon Generation~~~\n")
dg = DungeonGraph.DungeonGraph("Small")
dg.generateDungeon()
dg.drawDungeonGraph()
