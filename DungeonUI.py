import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk
import DungeonMapData as mapData
import DungeonGraph as dg


BG_IMG = "data/parchment.png"

class DungeonUI(tk.Tk):
	def __init__(self):
		tk.Tk.__init__(self)
		self.title("Your Dungeon Input")
		self.geometry("1200x600+300+300")
		
		self.container = tk.Frame(self)
		self.container.pack(side="top", fill="both", expand=True)
		self.container.grid_rowconfigure(0, weight=1)
		self.container.grid_columnconfigure(0, weight=1)
		
		self.inUI = DungeonIn(self.container, self)
		self.inUI.tkraise()

		
		#This will later take all the parameters to be used to create a dungeon
	def generateDungeonOut(self):
		self.title("Your Dungeon Output")
		self.inUI.destroy()
		
		dungeon = dg.DungeonGraph("Small")
		dungeon.generateDungeon()
		dungeon.populateDungeon()
		dungeon.printDungeon()


		dataMap = mapData.DungeonMapData(dungeon)
		dataMap.plotDungeon()
		
		dunOut = DungeonOut(self.container, dungeon.getDungeonInfoDictionary())
		dunOut.tkraise()

class DungeonIn(tk.Frame):
	def __init__(self, parent, controller):
		tk.Frame.__init__(self, parent)	 
		self.controller = controller
		
		self.initUI()
		
		
	def initUI(self):
		self.pack(fill=tk.BOTH, expand=True)
		
		#set background
		im = Image.open(BG_IMG)
		tkimage = ImageTk.PhotoImage(im)
		background_label = tk.Label(self, image=tkimage)
		background_label.image = tkimage
		background_label.place(x=0, y=0, relwidth=1, relheight=1)
		
		#configure Grid
		self.columnconfigure(0, weight=1, pad=300)
		self.columnconfigure(1, pad=150)
		self.columnconfigure(2, pad=100)
		self.columnconfigure(3, pad=150)
		self.columnconfigure(4, weight=1, pad=300)
		
		self.rowconfigure(0, weight=1, pad=100)
		
		self.rowconfigure(2, weight=1, pad=100)
		self.rowconfigure(4, weight=1, pad=100)
		
		self.rowconfigure(6, weight=1, pad=100)

		
		self.addDropDown()
		self.addButton()
		
	def addDropDown(self):
		#Size List
		self.size = tk.StringVar()
		sizes = ["Small", "Medium", "Large", "Huge"]
		self.size.set(sizes[0])
		
		self.sizeList = tk.OptionMenu(self, self.size, *sizes)
		self.sizeList.grid(row=1, column=1, sticky=tk.E+tk.W+tk.S+tk.N)
		
		#Type List
		self.type = tk.StringVar()
		types = ["Temple", "Mines", "Cult", "Stronghold", "Caves", "Forest", "Catacombs"]
		self.type.set(types[0])
		
		self.typeList = tk.OptionMenu(self, self.type, *types)
		self.typeList.grid(row=1, column=3, sticky=tk.E+tk.W+tk.S+tk.N)
		
		#Level List
		self.level = tk.StringVar()
		levels = []
		for i in range(1,11):
			levels.append(i)
			
		self.level.set(levels[0])
		
		self.levelList = tk.OptionMenu(self, self.level, *levels)
		self.levelList.grid(row=3, column=1, sticky=tk.E+tk.W+tk.S+tk.N)
		
		#Party Size
		self.partySize = tk.StringVar()
		pSizes = [4, 5, 6]
		self.partySize.set(pSizes[0])
		
		self.pSizeList = tk.OptionMenu(self, self.partySize, *pSizes)
		self.pSizeList.grid(row=3, column=3, sticky=tk.E+tk.W+tk.S+tk.N)
		
	def addButton(self):
		#grab all the parameters and pass it up.
		self.genButton = tk.Button(self, text="Generate Dungeon!!", command=self.controller.generateDungeonOut) #
		self.genButton.grid(row=5, column=2, sticky=tk.E+tk.W+tk.S+tk.N)
		pass

class DungeonOut(tk.Frame):
	def __init__(self,parent, dungeonInfo):
		tk.Frame.__init__(self, parent)
		self.imscale = 1.0
		self.imageid = None
		self.delta = 0.75
		self.dunInfo = dungeonInfo
		self.initUI()
		
		
	def initUI(self):
		self.pack(fill=tk.BOTH, expand=True)
		self.columnconfigure(0, weight=1, pad=5)
		self.columnconfigure(1)
		self.rowconfigure(1, weight=1)
		
		self.addDesc()
		self.addDropDown()
		self.addMapDisplay()
	
	def addDropDown(self):
		self.chosen = tk.StringVar()
		keys = list(self.dunInfo.keys())
		keys.sort()
		
		self.chosen.set(keys[0])
		self.chosen.trace("w", self.roomChanged)
		self.roomChanged()
		
		self.options = tk.OptionMenu(self, self.chosen, *keys)
		self.options.grid(row=0, column=1, sticky=tk.E+tk.W+tk.S+tk.N)
	
	def roomChanged(self, *args):
		currentRoom = int(self.chosen.get())
		self.roomDesc['text'] = self.dunInfo[currentRoom][0]
		
		if self.dunInfo[currentRoom][1] != "":
			self.roomInfo['text'] = self.dunInfo[currentRoom][1]
		else:
			self.roomInfo['text'] = "No Info Available"
	
	
	def addMapDisplay(self):
		self.map = tk.Canvas(self, width=400, height=600)
		self.map.grid(row = 0, column = 0, rowspan = 3, sticky=tk.E+tk.W+tk.S+tk.N)
		
		self.im = Image.open('map.png')
		self.mapimg = ImageTk.PhotoImage(self.im)
		
		self.imscale = 1.0
		self.imageid = None
		self.delta = 0.75
		
		tk.Widget.bind(self.map, "<1>", self.mouseDown)
		tk.Widget.bind(self.map, "<B1-Motion>", self.mouseMove)
		self.map.bind("<MouseWheel>",self.wheel)
		
		self.show_image()
		self.map.configure(scrollregion=self.map.bbox('all'))
	
	def addDesc(self):
		#room Desc
		im = Image.open(BG_IMG)
		im = im.resize((200, 300), Image.ANTIALIAS)
		tkimage = ImageTk.PhotoImage(im)
		self.roomDesc = tk.Label(self, compound=tk.CENTER, text="Temp Desc!!", image=tkimage, borderwidth=0, anchor="nw", wraplength=150)
		self.roomDesc.image = tkimage
		
		#Room Info
		self.roomInfo = tk.Label(self, compound=tk.CENTER, text="No Info!", image=tkimage, borderwidth=0, anchor="nw", wraplength=150)
		self.roomInfo.image = tkimage
	
		self.roomDesc.grid(row=1, column=1, sticky=tk.E+tk.W+tk.S+tk.N)
		self.roomInfo.grid(row=2, column=1, sticky=tk.E+tk.W+tk.S+tk.N)		
		
	#Code found at http://svn.python.org/projects/python/tags/r31/Demo/tkinter/matt/canvas-moving-w-mouse.py
	def mouseDown(self, event):
		# remember where the mouse went down
		self.lastx = event.x
		self.lasty = event.y

	def mouseMove(self, event):
		# whatever the mouse is over gets tagged as CURRENT for free by tk.
		self.map.move(tk.CURRENT, event.x - self.lastx, event.y - self.lasty)
		self.lastx = event.x
		self.lasty = event.y
		
	#Code used from https://stackoverflow.com/questions/25787523/move-and-zoom-a-tkinter-canvas-with-mouse
	def wheel(self, event):
		''' Zoom with mouse wheel '''
		scale = 1.0
		# Respond to Linux (event.num) or Windows (event.delta) wheel event
		if event.num == 5 or event.delta == -120:
			scale		 *= self.delta
			self.imscale *= self.delta
		if event.num == 4 or event.delta == 120:
			scale		 /= self.delta
			self.imscale /= self.delta
		# Rescale all canvas objects
		x = self.map.canvasx(event.x)
		y = self.map.canvasy(event.y)
		self.map.scale('all', x, y, scale, scale)
		self.show_image()
		self.map.configure(scrollregion=self.map.bbox('all'))

	def show_image(self):
		''' Show image on the Canvas '''
		if self.imageid:
			self.map.delete(self.imageid)
			self.imageid = None
			self.map.imagetk = None	 # delete previous image from the canvas
		width, height = self.im.size
		new_size = int(self.imscale * width), int(self.imscale * height)
		imagetk = ImageTk.PhotoImage(self.im.resize(new_size))
		# Use self.text object to set proper coordinates
		self.imageid = self.map.create_image(20,20, anchor='nw', image=imagetk)
		self.map.lower(self.imageid)  # set it into background
		self.map.imagetk = imagetk	# keep an extra reference to prevent garbage-collection

def main():
	root = DungeonUI()

	root.mainloop()	 
	

if __name__ == '__main__':
	main()	