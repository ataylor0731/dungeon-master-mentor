import math
import pandas as pd
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

import RoomTypes
import Encounters

class DungeonGraph:
	rooms = []
	idCounts = 2
	hallCounts = -1
	roomCounts = []
	occupiedSquares = [(0,0)]
	encounters = []

	def __init__(self, size):
		if size == "Small":
			self.roomCounts.append(("Entry", [1, 1]))
			self.roomCounts.append(("Large Hall", [0, 3]))
			self.roomCounts.append(("Encounter", [0, 3]))
			self.roomCounts.append(("Trap", [0, 2]))
			self.roomCounts.append(("Locked", [0, 1]))
			self.roomCounts.append(("Key", [0, 1]))
			self.roomCounts.append(("Loot", [0, 2]))
			self.roomCounts.append(("Secret", [0, 1]))
			self.roomCounts.append(("Empty", [0, 3]))
			self.roomCounts.append(("Small Hall", [0, math.inf]))
			self.roomCounts = dict(self.roomCounts)

		startRoom = RoomTypes.EntryRoom()
		startRoom.roomID = 1
		self.rooms.append(startRoom)

	def generateDungeon(self):
		roomPointer = 0
		while roomPointer < len(self.rooms):
			neighbors = self.getNeighbors(self.rooms[roomPointer].locRelToEntry)
			roomsToGenerate = self.rooms[roomPointer].generateChildRoomTypes(neighbors, self.roomCounts)

			#for every room, generate room object children
			#Check for neighbors first
			for room in roomsToGenerate[:]:
				#If open neighbor already exists, force a connection between them
				neighbor = self.getOpenNeighbor(neighbors, room)
				if neighbor != None and neighbor.roomID not in self.rooms[roomPointer].connectedRooms:
					self.connectNeighborRooms(self.rooms[roomPointer], neighbor)
					roomsToGenerate.remove(room)

			#Add new rooms second
			for room in roomsToGenerate[:]:
				openCoordinate = self.getFirstOpenCoordinate(self.rooms[roomPointer].locRelToEntry)
				if openCoordinate != None: #If there is an open space next to the room, create a new room there
					newRoom = self.createRoom(room, openCoordinate)
					self.connectRooms(self.rooms[roomPointer], newRoom)
					roomsToGenerate.remove(room)

			#Subtract failed room
			for room in roomsToGenerate:
				if room != None:
					self.roomCounts[room][0] -= 1

			roomPointer += 1
		self.cleanupFinalLayout()
		print(self.roomCounts)

	def createRoom(self, room, openCoordinate):

		#Failsafe check to ensure rooms cannot be created on other rooms
		if openCoordinate in self.occupiedSquares:
			return None

		newRoom = RoomTypes.generateRoomByTypeName(room)
		newRoom.setLocation(openCoordinate)
		if room == "Small Hall":
			newRoom.roomID = self.hallCounts
			self.hallCounts -= 1
		else:
			newRoom.roomID = self.idCounts
			self.idCounts += 1
		self.rooms.append(newRoom)
		self.occupiedSquares.append(openCoordinate)
		return newRoom

	def getNeighbors(self, roomLocation):
		neighbors = []
		for x in range(roomLocation[0] - 1, roomLocation[0] + 2):
			for y in range(roomLocation[1] - 1, roomLocation[1] + 2):
				if roomLocation[0] != x and roomLocation[1] != y:
					neighbor = self.getRoomByLocation((x, y))
					if neighbor != None:
						neighbors.append(neighbor)
		return neighbors

	def getRoomByLocation(self, location):

		for room in self.rooms:
			if room.locRelToEntry == location:
				return room
		return None

	def getRoomByID(self, id):
		for room in self.rooms:
			if room.roomID == id:
				return room
		return None

	def getOpenNeighbor(self, neighbors, roomType):
		for room in neighbors:
			if room.roomName == roomType and len(room.connectedRooms) < room.maxConnections:
				return room
		return None

	def connectRooms(self, parentRoom, childRoom):
		parentRoom.connectedRooms.append(childRoom.roomID)
		childRoom.connectedRooms.append(parentRoom.roomID)

	def connectNeighborRooms(self, parentRoom, childRoom):
		#Not actually adding room object yet
		#This function only marks an area where a hallway would be drawn as occupied
		if parentRoom.locRelToEntry[1] == childRoom.locRelToEntry[1]:
			connectRooms(parentRoom, childRoom)
			return

		if parentRoom.locRelToEntry[1] < childRoom.locRelToEntry[1]:
			newRoom = self.createRoom("Small Hall", (parentRoom.locRelToEntry[0], parentRoom.locRelToEntry[1] + 1))
		else:
			newRoom = self.createRoom("Small Hall", (childRoom.locRelToEntry[0], childRoom.locRelToEntry[1] + 1))

		if newRoom != None:
			self.connectRooms(parentRoom, newRoom)
			self.connectRooms(newRoom, childRoom)

	def getFirstOpenCoordinate(self, location):
		up = (0, 1)
		left = (-1, 0)
		right = (1, 0)
		directionOrder = []

		if location[0] == 0:
			directionOrder = [up, left, right]
		elif location[0] < 0:
			directionOrder = [left, up, right]
		else:
			directionOrder = [right, up, left]

		for direction in directionOrder:
			newLoc = (location[0] + direction[0], location[1] + direction[1])
			if newLoc not in self.occupiedSquares:
				return newLoc
		return None

	def cleanupFinalLayout(self):
		minLoc = (math.inf, 0)
		for loc in self.occupiedSquares:
			if loc[0] < minLoc[0]:
				minLoc = loc

		for room in self.rooms:
			room.setLocation((room.locRelToEntry[0] - minLoc[0], room.locRelToEntry[1]))

	def getMaxDimension(self):
		maxDim = -math.inf
		for room in self.rooms:
			if room.locRelToEntry[0] > maxDim:
				maxDim = room.locRelToEntry[0]
			if room.locRelToEntry[1] > maxDim:
				maxDim = room.locRelToEntry[1]
		return maxDim

	def getLargestRoomSize(self):
		largestRoomSize = -math.inf
		for room in self.rooms:
			if room.roomSize[0] > largestRoomSize or room.roomSize[1] > largestRoomSize:
				largestRoomSize = room.roomSize[0]
			elif room.roomSize[1] > largestRoomSize:
				largestRoomSize = room.roomSize[1]
		return largestRoomSize

	def getConnectingDirections(self, room):
		directions = {}

		roomRight = self.getRoomByLocation((room.locRelToEntry[0] + 1, room.locRelToEntry[1]))
		roomUp = self.getRoomByLocation((room.locRelToEntry[0], room.locRelToEntry[1] + 1))

		if roomRight != None and roomRight.roomID in room.connectedRooms:
			directions["Right"] = roomRight.roomID
		if roomUp != None and roomUp.roomID in room.connectedRooms:
			directions["Up"] = roomUp.roomID

		return directions

	def printDungeon(self):
		for room in self.rooms:
			room.printRoom()

	def populateDungeon(self):
		diffMod = 0
		
		for room in self.rooms:
			diffMod = room.generateEncounter(diffMod)
			room.generateRoomSize()
			room.generateDescription()

	def getDungeonInfoDictionary(self):
		info = {}
		for room in self.rooms:
			if room.roomID > 0:
				info[room.roomID] = (room.roomName + "\n\n" + room.description, room.encounter)

		return info

	def drawDungeonGraph(self):
		startPoints = []
		endPoints = []
		roomIDs = []
		roomTypes = []

		for room in self.rooms:
			roomIDs.append(room.roomID)
			roomTypes.append(room.roomName)

			for connection in room.connectedRooms:
				startPoints.append(room.roomID)
				endPoints.append(connection)

		#Construct Data Frame
		df = pd.DataFrame({ 'from':startPoints, 'to':endPoints})

		#Construct Characteristics
		carac = pd.DataFrame({'ID':roomIDs, 'values':roomTypes})

		# Build your graph
		G=nx.from_pandas_dataframe(df, 'from', 'to', create_using=nx.Graph())

		#Set Colors for room type
		carac= carac.set_index('ID')
		carac=carac.reindex(G.nodes())

		#Convert categories to numeric values
		carac['values']=pd.Categorical(carac['values'])
		carac['values'].cat.codes

		# Plot it
		nx.draw(G, with_labels=True, node_color=carac['values'].cat.codes, cmap=plt.cm.Set1, node_size=500, pos=nx.fruchterman_reingold_layout(G))

		plt.show()
