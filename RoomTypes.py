import Encounters
import random

def generateRoomByTypeName(roomType):
	if roomType == "Entry":
		return EntryRoom()
	if roomType == "Large Hall":
		return LargeHallRoom()
	if roomType == "Encounter":
		return EncounterRoom()
	if roomType == "Trap":
		return TrapRoom()
	if roomType == "Loot":
		return LootRoom()
	if roomType == "Locked":
		return LockedRoom()
	if roomType == "Key":
		return KeyRoom()
	if roomType == "Secret":
		return SecretRoom()
	if roomType == "Empty":
		return EmptyRoom()
	if roomType == "Small Hall":
		return SmallHallRoom()
	return None

class BaseRoom:
	def __init__(self):
		self.roomName = ""
		self.roomID = 0
		self.roomSize = (0,0)
		self.connectedRooms = []
		self.maxConnections = 0
		self.locRelToEntry = (0,0)
		self.drawCoords = (0,0)
		self.encounter = None
		self.description = None

	def generateChildRoomTypes(self, neighbors, roomCounts):
		possibleChildren = []

		for i in range(len(self.connectedRooms), self.maxConnections):
			newRoom = self.getRoom(Encounters.rollDice(1, 100))

			if self.checkRoomValid(neighbors, roomCounts, newRoom) and newRoom not in possibleChildren:
				possibleChildren.append(newRoom)
			else:
				newRoom = self.getFirstValidRoom(neighbors, roomCounts, possibleChildren)
				if newRoom != None:
					possibleChildren.append(newRoom)

		return possibleChildren

	def checkRoomValid(self, neighbors, roomCounts, roomType):
		if self.hasOpenNeighbor(neighbors, roomType):
			return True
		if roomCounts[roomType][0] < roomCounts[roomType][1]:
			roomCounts[roomType][0] += 1
			return True
		return False

	def hasOpenNeighbor(self, neighbors, roomType):
		for room in neighbors:
			if room.roomName == roomType and len(room.connectedRooms) < room.maxConnections:
				return True
		return False

	def printRoom(self):
		print(self.roomName)
		print("ID: " + str(self.roomID))
		print("Size: " + str(self.roomSize))
		print("Loc: " + str(self.locRelToEntry))
		print(str(self.connectedRooms))
		if self.description != None:
			print(self.description)
		if self.encounter != None:
			print(self.encounter)
		print("\n")

	def setLocation(self, coordinates):
		self.locRelToEntry = coordinates

	def setDrawCoords(self, coordinates):
		self.drawCoords = coordinates

	def getRoom(self, probability):
		pass

	def getFirstValidRoom(self, neighbors, roomCounts, possibleChildren):
		pass

	def generateRoomSize(self):
		pass
		
	def generateEncounter(self, diffMod):
		return diffMod
		
	def generateDescription(self):
		self.description = Encounters.getRoomDescription()

		
	

class EntryRoom(BaseRoom):
	def __init__(self):
		self.roomName = "Entry"
		self.roomID = 0
		self.roomSize = (0,0)
		self.connectedRooms = []
		self.maxConnections = 3
		self.locRelToEntry = (0,0)
		self.drawCoords = (0,0)
		self.encounter = None
		self.description = None

	def generateChildRoomTypes(self, neighbors, roomCounts):
		possibleChildren = []

		for i in range(len(self.connectedRooms), self.maxConnections):
			newRoom = self.getRoom(Encounters.rollDice(1, 100))

			if self.checkRoomValid(neighbors, roomCounts, newRoom):
				possibleChildren.append(newRoom)
			else:
				newRoom = self.getFirstValidRoom(neighbors, roomCounts, None)
				if newRoom != None:
					possibleChildren.append(newRoom)

		return possibleChildren

	def getRoom(self, probability):
		if probability <= 50:
			return "Large Hall"
		if probability <= 80:
			return "Encounter"
		return "Locked"

	def getFirstValidRoom(self, neighbors, roomCounts, possibleChildren):
		if self.checkRoomValid(neighbors, roomCounts, "Large Hall"):
			return "Large Hall"
		return None

	def generateRoomSize(self):
		size = random.randint(5, 7)
		self.roomSize = (size, size)


class LargeHallRoom(BaseRoom):
	def __init__(self):
		self.roomName = "Large Hall"
		self.roomID = 0
		self.roomSize = (0,0)
		self.connectedRooms = []
		self.maxConnections = 3
		self.locRelToEntry = (None,None)
		self.drawCoords = (None,None)
		self.encounter = None
		self.description = None

	def generateChildRoomTypes(self, neighbors, roomCounts):
		possibleChildren = []

		for i in range(len(self.connectedRooms), self.maxConnections):
			newRoom = self.getRoom(Encounters.rollDice(1, 100))

			if self.checkRoomValid(neighbors, roomCounts, newRoom):
				possibleChildren.append(newRoom)
			else:
				newRoom = self.getFirstValidRoom(neighbors, roomCounts, None)
				if newRoom != None:
					possibleChildren.append(newRoom)

		return possibleChildren

	def getRoom(self, probability):
		if probability <= 50:
			return "Encounter"
		if probability <= 80:
			return "Trap"
		if probability <= 90:
			return "Locked"
		return "Empty"

	def getFirstValidRoom(self, neighbors, roomCounts, possibleChildren):
		types = ["Encounter", "Trap", "Locked"]
		for room in types:
			if self.checkRoomValid(neighbors, roomCounts, room):
				return room

		if self.hasOpenNeighbor(neighbors, "Empty"):
			return "Empty"
		return None

	def generateRoomSize(self):
		size = random.randint(5, 7)
		self.roomSize = (size, size)


class EmptyRoom(BaseRoom):
	def __init__(self):
		self.roomName = "Empty"
		self.roomID = 0
		self.roomSize = (0,0)
		self.connectedRooms = []
		self.maxConnections = 3
		self.locRelToEntry = (None,None)
		self.drawCoords = (None,None)
		self.encounter = None
		self.description = None

	def generateChildRoomTypes(self, neighbors, roomCounts):
		newRoom = self.getRoom(Encounters.rollDice(1, 100))

		if self.checkRoomValid(neighbors, roomCounts, newRoom):
			return [newRoom]
		else:
			newRoom = self.getFirstValidRoom(neighbors, roomCounts, None)
			if newRoom != None:
				return [newRoom]
		return []

	def getRoom(self, probability):
		if probability <= 50:
			return "Encounter"
		if probability <= 90:
			return "Trap"
		return "Large Hall"

	def getFirstValidRoom(self, neighbors, roomCounts, possibleChildren):
		types = ["Encounter", "Trap"]
		for room in types:
			if self.checkRoomValid(neighbors, roomCounts, room):
				return room

		if self.hasOpenNeighbor(neighbors, "Large Hall"):
			return "Large Hall"
		return None

	def generateRoomSize(self):
		size = random.randint(3, 9)
		self.roomSize = (size, size)


class SecretRoom(BaseRoom):
	def __init__(self):
		self.roomName = "Secret"
		self.roomID = 0
		self.roomSize = (0,0)
		self.connectedRooms = []
		self.maxConnections = 2
		self.locRelToEntry = (None,None)
		self.drawCoords = (None,None)
		self.encounter = None
		self.description = None

	def generateChildRoomTypes(self, neighbors, roomCounts):
		newRoom = self.getRoom(Encounters.rollDice(1, 100))

		if self.checkRoomValid(neighbors, roomCounts, newRoom):
			return [newRoom]
		else:
			newRoom = self.getFirstValidRoom(neighbors, roomCounts, None)
			if newRoom != None:
				return [newRoom]
		return []

	def getRoom(self, probability):
		if probability <= 50:
			return "Loot"
		if probability <= 90:
			return "Encounter"
		return "Large Hall"

	def getFirstValidRoom(self, neighbors, roomCounts, possibleChildren):
		types = ["Encounter", "Trap"]
		for room in types:
			if self.checkRoomValid(neighbors, roomCounts, room):
				return room
		return "Loot"

	def generateRoomSize(self):
		size = random.randint(3, 5)
		self.roomSize = (size, size)


class EncounterRoom(BaseRoom):
	def __init__(self):
		self.roomName = "Encounter"
		self.roomID = 0
		self.roomSize = (0,0)
		self.connectedRooms = []
		self.maxConnections = 3
		self.locRelToEntry = (None,None)
		self.drawCoords = (None,None)
		self.encounter = None
		self.description = None

	def getRoom(self, probability):
		if probability <= 30:
			return "Key"
		if probability <= 60:
			return "Secret"
		if probability <= 90:
			return "Loot"
		return "Empty"

	def getFirstValidRoom(self, neighbors, roomCounts, possibleChildren):
		types = ["Key", "Secret", "Loot", "Empty"]
		for room in types:
			if self.checkRoomValid(neighbors, roomCounts, room):
				return room
		return None

	def generateRoomSize(self):
		size = random.randint(5, 7)
		self.roomSize = (size, size)
		
	def generateEncounter(self, diffMod):
		self.encounter = Encounters.getRandomEncounter(diffMod, Encounters.UNDERGROUND_1)
		return 10 + diffMod


class TrapRoom(BaseRoom):
	def __init__(self):
		self.roomName = "Trap"
		self.roomID = 0
		self.roomSize = (0,0)
		self.connectedRooms = []
		self.maxConnections = 3
		self.locRelToEntry = (None,None)
		self.drawCoords = (None,None)
		self.encounter = None
		self.description = None

	def getRoom(self, probability):
		if probability <= 30:
			return "Loot"
		if probability <= 60:
			return "Encounter"
		if probability <= 90:
			return "Key"
		return "Empty"

	def getFirstValidRoom(self, neighbors, roomCounts, possibleChildren):
		types = ["Loot", "Encounter", "Key", "Empty"]
		for room in types:
			if self.checkRoomValid(neighbors, roomCounts, room):
				return room
		return "Empty"

	def generateRoomSize(self):
		size = random.randint(3, 5)
		self.roomSize = (size, size)
		
	def generateEncounter(self, diffMod):
		self.encounter = Encounters.getRandomTrap(diffMod, 1)
		return 10 + diffMod


class LockedRoom(BaseRoom):
	def __init__(self):
		self.roomName = "Locked"
		self.roomID = 0
		self.roomSize = (0,0)
		self.connectedRooms = []
		self.maxConnections = 2
		self.locRelToEntry = (None,None)
		self.drawCoords = (None,None)
		self.encounter = None
		self.description = None

	def getRoom(self, probability):
		if probability <= 50:
			return "Loot"
		if probability <= 70:
			return "Encounter"
		if probability <= 90:
			return "Large Hall"
		return "Empty"

	def getFirstValidRoom(self, neighbors, roomCounts, possibleChildren):
		types = ["Loot", "Encounter", "Large Hall", "Empty"]
		for room in types:
			if self.checkRoomValid(neighbors, roomCounts, room):
				return room
		return "Loot"

	def generateRoomSize(self):
		size = random.randint(5, 7)
		self.roomSize = (size, size)


class LootRoom(BaseRoom):
	def __init__(self):
		self.roomName = "Loot"
		self.roomID = 0
		self.roomSize = (0,0)
		self.connectedRooms = []
		self.maxConnections = 2
		self.locRelToEntry = (None,None)
		self.drawCoords = (None,None)
		self.encounter = None
		self.description = None

	def getRoom(self, probability):
		if probability <= 40:
			return "Large Hall"
		if probability <= 80:
			return "Encounter"
		return "Empty"

	def getFirstValidRoom(self, neighbors, roomCounts, possibleChildren):
		if self.checkRoomValid(neighbors, roomCounts, "Empty"):
			return "Empty"
		return None

	def generateRoomSize(self):
		size = random.randint(5, 9)
		self.roomSize = (size, size)
		
	def generateEncounter(self, diffMod):
		self.encounter = Encounters.getLoot()
		return diffMod


class KeyRoom(BaseRoom):
	def __init__(self):
		self.roomName = "Key"
		self.roomID = 0
		self.roomSize = (0,0)
		self.connectedRooms = []
		self.maxConnections = 3
		self.locRelToEntry = (None,None)
		self.drawCoords = (None,None)
		self.encounter = None
		self.description = None

	def getRoom(self, probability):
		if probability <= 40:
			return "Large Hall"
		if probability <= 80:
			return "Encounter"
		return "Empty"

	def getFirstValidRoom(self, neighbors, roomCounts, possibleChildren):
		if self.checkRoomValid(neighbors, roomCounts, "Empty"):
			return "Empty"
		return None

	def generateRoomSize(self):
		size = random.randint(3, 5)
		self.roomSize = (size, size)

class SmallHallRoom(BaseRoom):
	def __init__(self):
		self.roomName = "Small Hall"
		self.roomID = 0
		self.roomSize = (1,1)
		self.connectedRooms = []
		self.maxConnections = 2
		self.locRelToEntry = (None,None)
		self.drawCoords = (None,None)
		self.encounter = None
		self.description = None

	def generateChildRoomTypes(self, neighbors, roomCounts):
		return []
